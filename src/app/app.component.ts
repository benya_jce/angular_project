import { Component, EventEmitter, Output, Inject} from '@angular/core';
import{AngularFire} from 'angularfire2';
import { MessagesService } from "./home-page/messages.service";
import { Router } from "@angular/router";
import { FirebaseApp } from 'angularfire2';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  public isLoggedIn: boolean;
  img: string;

storageRef;
firebase:any;
constructor(public afService: MessagesService, private router: Router, @Inject(FirebaseApp) firebase: any) {
    this.firebase = firebase;
    this.storageRef = firebase.storage().ref().child('profile.png');
        this.storageRef.getDownloadURL().then((urll) => {
            this.img = urll;
            
        }
        );
    this.afService.af.auth.subscribe(
      (auth) => {
        if(auth == null) {
          console.log("Not Logged in.");
          this.isLoggedIn = false;
          this.router.navigate(['login']);
        }
        else {
          console.log("Successfully Logged in.");
    
         
          if(auth.google) {
            this.afService.displayName = auth.google.displayName;
            this.afService.email = auth.google.email;
            this.afService.photo = auth.google.photoURL;
            this.img = auth.google.photoURL;
          }
          else {
            this.afService.displayName = auth.auth.email;
            this.afService.email = auth.auth.email;
            this.afService.photo =this.img;

          }
          this.isLoggedIn = true;
          this.router.navigate(['']);
        }
      }
    );
    // this.afService.addUserInfo1(auth.google.email,auth.google.displayName,auth.google.photoURL);
  }
  logout() {
    this.afService.logout();
  }
}
