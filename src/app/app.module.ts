import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import{AngularFireModule} from 'angularfire2';

import { RouterModule, Routes } from '@angular/router';

import { RoomService } from './room/room.service';
import { MessagesService } from './home-page/messages.service';

import { ChatComponent } from './chat/chat.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { RegistrationPageComponent } from './registration-page/registration-page.component';
import { RoomComponent } from './room/room.component';
import { NewChatFormComponent } from './new-chat-form/new-chat-form.component';
import { ManegeMembersComponent } from './manege-members/manege-members.component';


export const firebaseConfig = {
    apiKey: "AIzaSyD0RxOhT86-2KdcnIn8RX9TJvbLBy8I_dI",
    authDomain: "angular-learning-58f55.firebaseapp.com",
    databaseURL: "https://angular-learning-58f55.firebaseio.com",
    projectId: "angular-learning-58f55",
    storageBucket: "angular-learning-58f55.appspot.com",
    messagingSenderId: "115690403547"
};

const appRoutes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'chat/:roomkey/:titleroom', component: ChatComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'chatForm', component: NewChatFormComponent },
  { path: 'register', component: RegistrationPageComponent},
  { path: 'chatForm/:roomKey/:roomName', component: ManegeMembersComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    LoginPageComponent,
    HomePageComponent,
    RegistrationPageComponent,
    RoomComponent,
    NewChatFormComponent,
    ManegeMembersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig),
    FormsModule,
 
  ],

  providers: [MessagesService,RoomService],
  bootstrap: [AppComponent]
})
export class AppModule { }
