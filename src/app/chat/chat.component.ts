import {Component, OnInit, AfterViewChecked, ElementRef, ViewChild} from '@angular/core';
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2';
import {ActivatedRoute} from '@angular/router';
import {MessagesService} from "../home-page/messages.service";
import { HomePageComponent } from '../home-page/home-page.component';
import {Router} from "@angular/router";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, AfterViewChecked  {

  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  public chatTitle: string;
  public roomID :String;
  public roomAdmin :String;
  public newMessage: string;
  adminName:string;
  public userId: string;
  public messages: FirebaseListObservable<any>;
  members={};
  memberList=[];

  constructor(private router: Router,private af: AngularFire,public afService: MessagesService, private AR:ActivatedRoute) {
    this.messages = this.afService.messages;
    this.chatTitle = this.AR.snapshot.params['titleroom'];
    this.roomID = this.AR.snapshot.params['roomkey'];
    this.af.auth.subscribe(user => {
        if (user) {
        this.userId = user.uid;

        } else {
          this.userId =null;
        }
    });

     
     this.af.database.object('room/' + this.roomID).$ref.on("value",(snapshot) => {
         this.members  = snapshot.val().members;
         this.roomAdmin = snapshot.val().admin;
            var Aemail;
            var Aphoto;
            var Aname;

         this.af.database.object('registeredUsers/' + snapshot.val().admin).$ref.on("value",(snapshotuser) => {
                Aemail  = snapshotuser.val().email;
                Aphoto  = snapshotuser.val().photo; 
                 Aname  = snapshotuser.val().name; 
                this.memberList.push({userId:snapshot.val().admin ,email: Aemail, photo: Aphoto, name: Aname, admin: 1});
            });
         for(var p in this.members){
            var email;
            var photo;
            var name;

            this.af.database.object('registeredUsers/' + p).$ref.on("value",(snapshotuser) => {
                email  = snapshotuser.val().email;
                photo  = snapshotuser.val().photo; 
                 name  = snapshotuser.val().name; 
                this.memberList.push({userId:p ,email: email, photo: photo, name: name, admin: 0});
            });
          }
         });
 
  }
  ngOnInit() {}
  leaving(){
    var answer = confirm("To return to the"+ this.chatTitle +" room will have to ask the Admin");
    if(answer)
    {
     
      delete this.members[this.userId];
      var room;
      room={ members: this.members}
       this.af.database.object('/room/' + this.roomID).update(room);
        this.router.navigate(['']);
    }

    
  }
  isYou(email) {
    if(email == this.afService.email)
      return true;
    else
      return false;
  }
  isMe(email) {
    if(email == this.afService.email)
      return false;
    else
      return true;
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch(err) { console.log('Scroll to bottom failed') }
  }
  

  sendMessage(){

    this.afService.sendMessage(this.newMessage,this.roomID);
    this.newMessage = '';
  }

  
 

}

