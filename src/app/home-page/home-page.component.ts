import {Component, OnInit, AfterViewChecked, ElementRef, ViewChild} from '@angular/core';
import {MessagesService} from "./messages.service";
import {RoomService} from "../room/room.service";
import {FirebaseListObservable} from "angularfire2";
import {ChatComponent} from "../chat/chat.component";
import {Router} from "@angular/router";
import { FirebaseApp } from 'angularfire2';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit  {
public chatRooms: FirebaseListObservable<any>;
userId: string;
group;

  constructor(public afService: MessagesService ,private afRoomService: RoomService,private router: Router) {
      this.chatRooms = this.afRoomService.chatRooms;
      this.userId = this.afService.logInUserId;

      

        
  }

  delete( ){
    console.log(1)
  }
  Leave(){

  }

  memberId(room){
    for(var p in room.members){
     if(p==this.afService.logInUserId){
       return true;
     }
   }
   return false;
    
  }

  manegeOf(room){
     if(room.admin==this.afService.logInUserId){
       return true;
     }
   return false;
    
  }
  ngOnInit() {}


}

