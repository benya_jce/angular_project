import {Injectable,Inject} from "@angular/core";
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable,FirebaseApp} from 'angularfire2';
import {FirebaseObjectFactoryOpts} from "angularfire2/interfaces";

@Injectable()
export class MessagesService {
  public messages: FirebaseListObservable<any>;
  public logInUserId : string;
  public users: FirebaseListObservable<any>;
  public displayName: string;
  public email: string;
  public photo: string;
  public user: FirebaseObjectObservable<any>;
  img: string;
  
  storageRef;
  firebase:any;

  constructor(public af: AngularFire, @Inject(FirebaseApp) firebase: any) {
      this.af.auth.subscribe(
      (auth) => {
        if (auth != null) {
          this.user = this.af.database.object('registeredUsers/' + auth.uid);
        }
      });

    this.messages = this.af.database.list('messages');
    this.users = this.af.database.list('users');
    this.af.auth.subscribe(user => {
        if (user) {
        this.logInUserId = user.uid;

        } else {
          this.logInUserId =null;
        }
    });


    this.firebase = firebase;
    

    this.storageRef = firebase.storage().ref().child('profile.png');
        this.storageRef.getDownloadURL().then((urll) => {
            this.img = urll;
         

        }
            
           
        );

   }
  /**
   * Logs in the user
   * @returns {firebase.Promise<FirebaseAuthState>}
   */
  loginWithGoogle() {
      
    return this.af.auth.login({
      provider: AuthProviders.Google,
      method: AuthMethods.Popup,
    });
  }
  /**
   * Logs out the current user
   */
  logout() {
    return this.af.auth.logout();
  }
  /**
   * Saves a message to the Firebase Realtime Database
   * @param text
   */


  sendMessage(text,roomID) {
    var message = {
      message: text,
      displayName: this.displayName,
      email: this.email,
      photo: this.photo,
      timestamp: Date.now(),
      room: roomID
    };
    this.messages.push(message);
  }

  addUserInfo(){
    //We saved their auth info now save the rest to the db.
    this.users.push({
      email: this.email,
      displayName: this.displayName
    });
  }

  saveUserInfoFromGoogle(data){
    //We saved their auth info now save the rest to the db.
    this.af.database.object('registeredUsers/' + data.uid).set({
      name: this.displayName,
      email: this.email,
      photo: this.photo,
    });
  }

    registerUser(email, password) {
        console.log(email)
        return this.af.auth.createUser({
        email: email,
        password: password
        });
    }

    saveUserInfoFromForm(uid, name, email) {
    return this.af.database.object('registeredUsers/' + uid).set({
      name: name,
      email: email,
      photo: this.img
    });
  }

  /**
   * Logs the user in using their Email/Password combo
   * @param email
   * @param password
   * @returns {firebase.Promise<FirebaseAuthState>}
   */
  loginWithEmail(email, password) {
    return this.af.auth.login({
        email: email,
        password: password,
      },
      {
        provider: AuthProviders.Password,
        method: AuthMethods.Password,
      });
  }

  check(){

        console.log(this.user)

  }


  


  
}

