import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManegeMembersComponent } from './manege-members.component';

describe('ManegeMembersComponent', () => {
  let component: ManegeMembersComponent;
  let fixture: ComponentFixture<ManegeMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManegeMembersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManegeMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
