import { Component, OnInit } from '@angular/core';
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2';
import {MessagesService} from '../home-page/messages.service';
import { NgForm } from '@angular/forms';
import {Router} from "@angular/router";
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-manege-members',
  templateUrl: './manege-members.component.html',
  styleUrls: ['./manege-members.component.css']

})
export class ManegeMembersComponent implements OnInit {

  roomToUpdate: FirebaseObjectObservable<any>;
  roomId = null;
  users :FirebaseListObservable<any>;
  rooms :FirebaseListObservable<any>;
  userId;
  memberList=[];
  members={};
  roomTitle:string;
  checktitle = 0;
  checkmembers = 0;


  constructor(public af: AngularFire, private router: Router,private AR:ActivatedRoute) { 
    this.users = this.af.database.list('registeredUsers');
    this.rooms = this.af.database.list('room');
    this.af.auth.subscribe(user => {
        if (user) {
        this.userId = user.uid;

        } else {
          this.userId =null;
        }
    });

    this.roomId = this.AR.snapshot.params['roomKey'];
    this.roomTitle = this.AR.snapshot.params['roomName'];
    this.roomToUpdate = this.af.database.object('room/' + this.roomId);
    

   
    if(this.roomId != null){
      this.roomToUpdate.$ref.on("value",(snapshot) => {
         this.members  = snapshot.val().members; 
          for(var p in this.members){
            var email;
            var photo;
            this.af.database.object('registeredUsers/' + p).$ref.on("value",(snapshotuser) => {
                email  = snapshotuser.val().email;
                photo  = snapshotuser.val().photo; 
                this.memberList.push({userId:p ,email: email, photo: photo});
            });
          }
        });
    }

    
  }

  checkTitle(){
    this.checktitle = 0;
  }



  addMember(t){
    this.checkmembers = 0;
    if(!this.members[t.$key]){
      this.memberList.push({userId:t.$key ,email: t.email, photo: t.photo});
    }
    this.members[t.$key] = true;
     console.log(this.memberList);
  }

  update()
  {
    if(this.roomTitle == '' || this.roomTitle == null || this.memberList.length == 0){
      if(this.roomTitle == '' || this.roomTitle == null){
        this.checktitle =1
      }
      if(this.memberList.length == 0){
      this.checkmembers =1
      }      
      return;
    }
    var room ={
    admin :this.userId,
    members: this.members,
    title: this.roomTitle
    }
    this.af.database.object('room/'+ this.roomId).update(room);
    this.router.navigate(['/chat/',this.roomId,this.roomTitle]);
    
    this.members ={};
    this.memberList = [];
    this.roomTitle = '';
    
  }

  deleteUser(user)
  {
    console.log(this.members)
    let index: number = this.memberList.indexOf(user);
    if (index !== -1) {
        this.memberList.splice(index, 1);
      console.log(user.userId)
      delete this.members[user.userId];
  
    }       
    
  }

  ngOnInit() {
    
    
  }

}
