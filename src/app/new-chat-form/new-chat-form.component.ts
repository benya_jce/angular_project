import { Component, OnInit } from '@angular/core';
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2';
import {MessagesService} from '../home-page/messages.service';
import { NgForm } from '@angular/forms';
import {Router} from "@angular/router";

@Component({
  selector: 'app-new-chat-form',
  templateUrl: './new-chat-form.component.html',
  styleUrls: ['./new-chat-form.component.css']
})
export class NewChatFormComponent implements OnInit {
  
  users :FirebaseListObservable<any>;
  userId;
  memberList=[];
  members = {};
  roomTitle:string;
  checktitle = 0;
  checkmembers=0;
  
  constructor(public af: AngularFire, private router: Router) { 
    this.users = this.af.database.list('registeredUsers');
    this.af.auth.subscribe(user => {
        if (user) {
        this.userId = user.uid;

        } else {
          this.userId =null;
        }
    });
    
  }

  checkTitle(){
    this.checktitle = 0;
  }


  addMember(t){
  
    this.checkmembers = 0;
    if(!this.members[t.$key]){
      this.memberList.push({userId:t.$key ,email: t.email, photo: t.photo});
    }
    this.members[t.$key] = true;
     console.log(this.memberList);
  }

  create()
  {
    if(this.roomTitle == '' || this.roomTitle == null || this.memberList.length == 0){
      if(this.roomTitle == '' || this.roomTitle == null){
        this.checktitle =1
      }
      if(this.memberList.length == 0){
      this.checkmembers =1
      }      
      return;
    }
  
    var room ={
    admin :this.userId,
    date :  Date.now(),
    members: this.members,
    title: this.roomTitle
    }
    this.af.database.list('room').push(room);
    

const users = this.af.database.list('/room', 
      {
        //filter by email key
        query : {
          orderByChild: 'date',
          equalTo : room.date
          } 
      });
      users.subscribe(data => {
        if(data.length > 0)
        {
          this.router.navigate(['/chat/',data["0"].$key,room.title]);
        }
      });

    
    this.members ={};
    this.memberList = [];
    this.roomTitle = '';
    
  }

  deleteUser(user)
  {
    console.log(this.members)
    let index: number = this.memberList.indexOf(user);
    if (index !== -1) {
        this.memberList.splice(index, 1);
      console.log(user.userId)
      delete this.members[user.userId];
  
    }       
    
  }

  ngOnInit() {
    
  }

}
