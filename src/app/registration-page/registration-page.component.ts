import { Component } from '@angular/core';
import {MessagesService} from "../home-page/messages.service";
import {Router} from "@angular/router";
@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.css']
})
export class RegistrationPageComponent {
  public error: any;
  constructor(private afService: MessagesService, private router: Router) { }
    //registers the user and logs them in
    register(event, name, email, password) {
      event.preventDefault();
      this.afService.registerUser(email, password).then((user) => {
        this.afService.saveUserInfoFromForm(user.uid, name, email).then(() => {
          this.router.navigate(['']);
        })
          .catch((error) => {
            this.error = error;
          });
      })
        .catch((error) => {
          this.error = error;
          console.log(this.error);
        });
    }
}

