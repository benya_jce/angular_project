import { Injectable } from '@angular/core';
import {AngularFire, AuthProviders, AuthMethods, FirebaseListObservable, FirebaseObjectObservable} from 'angularfire2';
import {FirebaseObjectFactoryOpts} from "angularfire2/interfaces";

@Injectable()

export class RoomService {
  public memberIn;
  public adminOf;
  public chatRooms: FirebaseListObservable<any>;
  public getRoom: FirebaseObjectObservable<any>;



  constructor(public af: AngularFire) {
     this.chatRooms = this.af.database.list('room');
     
   }
    

    getRoomProperty(roomId){
       this.getRoom.$ref.on("value", function(snapshot) {
         var room = snapshot.val();
           return room;
        });
      
    }
}
